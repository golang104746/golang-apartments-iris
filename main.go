package main

import (
	"apartments-server/routes"
	"apartments-server/storage"

	"github.com/go-playground/validator/v10"
	"github.com/joho/godotenv"
	"github.com/kataras/iris/v12"
)

func main() {
	godotenv.Load()
	storage.InitializeDb()

	// initialize app with new instance of iris
	app := iris.Default()
	app.Validator = validator.New()

	// default endpoint
	location := app.Party("api/location")
	// http://localhost:4000/api/location/autocomplete?location=San_Diego&limit=2
	// http://localhost:4000/api/location/search?location=San_Diego

	// endpoints
	{
		location.Get("/autocomplete", routes.Autocomplete)
		location.Get("/search", routes.Search)
	}
	user := app.Party("/api/user")
	{
		user.Post("/register", routes.Register)
		user.Post("/register", routes.Login)
	}

	app.Listen(":4000")
}