package models

import "gorm.io/gorm"

// structure for creating User db table/instance
type User struct {
	gorm.Model
	// ID
	// UpdatedAt
	// DeletedAt
	// CreatedAt
	FirstName string `json:"firstName"` // specifies how it looks in json format
	LastName string `json:"lastName"`
	Email string `json:"email"`
	Password string `json:"password"`
	SocialLogin bool `json:"socialLogin"`
	SocialProvider string `json:"socialProvider"`
}