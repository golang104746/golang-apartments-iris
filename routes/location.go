package routes

import (
	"encoding/json"
	"io"
	"net/http"
	"os"

	"github.com/kataras/iris/v12"
)

// calls the autocomplete/search endpoints from location IQ API
// ctx iris.Context - represents context of a Http request/holds info about the request
func Autocomplete(ctx iris.Context) {
	limit := "10" // initializes var named limit, sets limit to 10 by default
	location := ctx.URLParam("location")// gets location param passed in from url
	limitQuery := ctx.URLParam("limit") // limit param passed in to url
	// if limit parameter is provided in the URL, override default limit
	if limitQuery != "" {
		limit = limitQuery
	}

	apiKey := os.Getenv("LOCATION_TOKEN")
	url := "https://api.locationiq.com/v1/autocomplete?key=" + apiKey + "&q=" + location + "&limit=" + limit

	fetchLocations(url, ctx)
}

// search based on location
func Search(ctx iris.Context) {
	location := ctx.URLParam("location")

	apiKey := os.Getenv("LOCATION_TOKEN")
	url :=
		"https://api.locationiq.com/v1/search?key=" + apiKey + "&q=" + location + "&format=json&dedupe=1&addressdetails=1&matchquality=1&normalizeaddress=1&normalizecity=1"

	fetchLocations(url, ctx)
}

func fetchLocations(url string, ctx iris.Context) {
	client := &http.Client{} // represents api client - used to send quest to autocomplete API
	// call the server by creating a new get request and passing in the URL
	req, _ := http.NewRequest("GET", url, nil) // new http req obj
	res, locationErr := client.Do(req)  // sends http req and returns response(res)
	// handle any http req errors
	if locationErr != nil {
		// 500 err and err message(title & detail)
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().
			Title("Internal Server Error").
			Detail("Internal Server Error"))
		return
	}

	defer res.Body.Close() // cleanup to close res body

	body, bodyErr := io.ReadAll(res.Body)
    // read the body of the response
	if bodyErr != nil {
		// 500 server error & message
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().
			Title("Internal Server Error").
			Detail("Internal Server Error"))
		return
	}

	var objMap []map[string]interface{} // collection of string hash maps
	jsonErr := json.Unmarshal(body, &objMap) // decode json data from body into objMap
	if jsonErr != nil { // check for errors
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().
			Title("Internal Server Error").
			Detail("Internal Server Error"))
		return
	}

	ctx.JSON(objMap) // return api response
}