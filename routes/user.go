package routes

import (
	"apartments-server/models"
	"apartments-server/storage"
	"apartments-server/utils"
	"strings"

	"github.com/kataras/iris/v12"
	"golang.org/x/crypto/bcrypt"
)

// http://localhost:4000/api/user/register
// { "firstName": "test", "lastName": "test", "email": "test@gmail.com", "password": "password" }

// create new user - expects json data from the incoming req body
func Register(ctx iris.Context) {
	var userInput RegisterUserInput
	err := ctx.ReadJSON(&userInput) // decode/read json from userInput struct
	if err != nil {
        // fmt.Println(err.Error())
		utils.HandleValidationErrors(err, ctx)
		return
    }

	var newUser models.User // create new user model
	userExists, userExistsErr := getAndHandleUserExists(&newUser, userInput.Email)
	if userExistsErr != nil {
		// fmt.Println(userExistsErr.Error())
		utils.CreateInternalServerError(ctx)
		return
	}

	if userExists {
		// fmt.Println("User already exists")
		utils.CreateError(
			iris.StatusConflict, // 409
			"Conflict",
			"Email already registered",
			ctx,
		)
		return
	}

	// if user does not exist, hash and salt password and store info in db
	hashedPassword, hashErr := hashAndSaltPassword(userInput.Password)
	if hashErr != nil {
		// fmt.Println(hashErr.Error())
		utils.CreateInternalServerError(ctx)
		return
	}

	// save user to db and returns basic info about the newly created user in json format
	newUser = models.User{
		FirstName:   userInput.FirstName,
        LastName:    userInput.LastName,
        Email:       strings.ToLower(userInput.Email),
        Password:    hashedPassword,
        SocialLogin: false,
	}
	storage.DB.Create(&newUser)

	ctx.JSON(iris.Map{
		"ID":        newUser.ID,
		"firstName": newUser.FirstName,
		"lastName":  newUser.LastName,
		"email":     newUser.Email,
	})
}

func Login(ctx iris.Context) {
	var userInput LoginUserInput // user input - email/password
	err := ctx.ReadJSON(&userInput) // read json from user input
	if err != nil { // handle validation errors
		utils.HandleValidationErrors(err, ctx)
		return
	}

	// handle incorrect email/password
	var existingUser models.User
	errorMsg := "Invalid email or password"
	userExists, userExistsErr := getAndHandleUserExists(&existingUser, userInput.Email)
	if userExistsErr != nil {
        utils.CreateInternalServerError(ctx)
        return
    }

	// if user hasn't registered or gives bad credentials
	if !userExists {
        utils.CreateError(iris.StatusUnauthorized, "Credentials Error", errorMsg, ctx)
        return
    }

	// Questionable as to wherever you should let userInput know they logged in with 0auth
	// below code can be commented out if this is unwanted behaviour
	if existingUser.SocialLogin {
		utils.CreateError(iris.StatusUnauthorized, "Credentials Error", "Social Login Account", ctx)
		return
	}

	// if email/user exists - compare password user sent us with password in db
	passwordErr := bcrypt.CompareHashAndPassword([]byte(existingUser.Password), []byte(userInput.Password))
	// pwd err
	if passwordErr != nil {
		utils.CreateError(iris.StatusUnauthorized, "Credentials Error", errorMsg, ctx)
		return
	}

	ctx.JSON(iris.Map{
		"ID":        existingUser.ID,
		"firstName": existingUser.FirstName,
		"lastName":  existingUser.LastName,
		"email":     existingUser,
	})
}

// check if user is already registered - accepts pointer to user model and email passed in
func getAndHandleUserExists(user *models.User, email string) (exists bool, err error) {
	// query db
	userExistsQuery := storage.DB.Where("email = ?", strings.ToLower(email)).Limit(1).Find(&user)

	// Check if user already exists:

	// if error occurs getting the query, return false alongside error
	if userExistsQuery.Error != nil {
		return false, userExistsQuery.Error
	}

	// evaluates if the query affected any rows in the db and sets to true if so, if greater than 0 user does exist
	userExists := userExistsQuery.RowsAffected > 0

	if userExists {
		return true, nil
	}

	// return false if user does not exist and no errors
	return false, nil
}


// data structure for register input
type RegisterUserInput struct {
	FirstName string `json:"firstName" validate:"required,max=256"`
	LastName  string `json:"lastName" validate:"required,max=256"`
	Email     string `json:"email" validate:"required,max=256,email"`
	Password  string `json:"password" validate:"required,min=8,max=256"`
}


func hashAndSaltPassword(password string) (hashedPassword string, err error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

type LoginUserInput struct {
	Email    string `json:"email" validate:"required.email"`
	Password string `json:"password" validate:"required"`
}